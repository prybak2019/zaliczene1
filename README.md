Aby uruchomić naszą usługę należy
pobrać wszystkie pliki w wybraną teczkę na swoim dysku trwardym.
Kolejnie należy w konsoli wykonać polecenia:
npm install --save cors
npm install --save socket-io
npm install --save express
node dialer.js
Kolejnie należy otworzyć stronę sieciową https://restninja.io/ koniecznie
w przeglądarce Chrome z wstanowioną usługą cors. Wybrać w prawym górnym rogu
AJAX, po lewo POST, wpisać adres localhost:3000/call, 
w headers wybrać content-type:application/json,
w body wybrać 
{    "number1": "xxx",    "number2": "xxx2"}, gdzie
w miejsca xxx oraz xxx2 są podane numery obu telefonów.
w konsoli wyświeli się nam status połączenia a w przeglądarce 
otrzymamy oba id zestawionych połączeń.
Aby usługa działała należy z początku odznaczyć wiersz 
//let connector = ConnectorFactory.create(config)
w miejsce tego z następnego wiersza.