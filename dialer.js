const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
const cors = require('cors');
app.use(cors());
const http = require('http').Server(app);
const io = require('socket.io')(http);
const ConnectorFactory = require('dialer').ConnectorFactory
const config = {
 url: 'https://uni-call.fcc-online.pl',
 login: 'focus04',
 password: '24twgfdk1s'
};
//let connector = ConnectorFactory.create(config)
let connector = ConnectorFactory.create()

app.listen(3000, () => {
 console.log('app listening on port 3000');
});
app.post('/call', async (req, res) => {
 let result1 = await connector.call(req.body.number1)
 let result2 = await connector.call(req.body.number2)
 let status1 = await connector.getStatus(result1.id)
 let status2 = await connector.getStatus(result2.id)
 if(status1.status=='FAILED' || status2.status=='FAILED'){
  res.json({success: false});
 }else{
  io.emit('status', status1.status)
  io.emit('status', status2.status)
  let interval = setInterval(async () => {      
   console.log('sprawdzam w interwale')  
   status1 = await connector.getStatus(result1.id)
   status2 = await connector.getStatus(result2.id)
   if(status1.status!='RINGING'&&status2.status!='RINGING'){
    io.emit('status', status1.status)
    io.emit('status', status2.status)
    clearInterval(interval)
    await connector.bridge(result1.id, result2.id)
   }
   console.log(status1.status)
   console.log(status2.status)
  },500)
  res.json({success: true, callId1: result1.id, callId2: result2.id});	 
 }
})

app.get('/status/:callsId', async (req, res) => {
 let status1 = await connector.getStatus(req.params.callsId)
 if(typeof status1.success !== 'undefined'){
  res.json({success: false});	 
 }else{
  res.json({success: true, status: status1.status});	 
 }
})
